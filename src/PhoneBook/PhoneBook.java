package PhoneBook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PhoneBook {
	ArrayList<Contract> contracts = new ArrayList<Contract>();
	public PhoneBook() {
		String filename = "phonebook.txt";
		String name = "";
		String number = "";
		Contract contract;
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				name = data[0].trim();
				number = data[1].trim();
				
				contract = new Contract(name, number);
				contracts.add(contract);
			}
			
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}	
	}
	
	public String toString(){
		String str = "";
		for (Contract contract : contracts) {
			str += contract.getName() + " " + contract.getNumber() + "\n";
		}
		return str;
	}
	
}
