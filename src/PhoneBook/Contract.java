package PhoneBook;

public class Contract {
	private String name;
	private String number;
	
	public Contract(String name, String number) {
		this.name = name;
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public String getNumber() {
		return number;
	}
	
	
}
