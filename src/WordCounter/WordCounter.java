package WordCounter;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class WordCounter {
	private HashMap<String,Integer> wordCount;
	private ArrayList<String> strings = new ArrayList<String>();
	public WordCounter() {
		String filename = "poem.txt";
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			String[] data = null;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				data = line.split(",");
			}
			for (String string : data) {
				strings.add(string);
			}	
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}	
	}
	
	public void count(){
		wordCount = new HashMap<String,Integer>();
		int value = 1;
		for (String str : strings) {
			if (wordCount.containsKey(str)){
				value = wordCount.get(str);
				wordCount.replace(str, value+1);
			}
			else {
				wordCount.put(str, 1);
			}
			
		}
	}
	
	public String getCountData(){
		String str = "";
		 Set<Map.Entry<String, Integer>> entries = wordCount.entrySet();
	 	 for (Map.Entry<String, Integer> entry : entries) {
			 String key = entry.getKey();
			 str += key + " " + "=" + " " + entry.getValue() + "\n";
	 	 }
	 	return str;
		
	
		
	}
}
