package examScore;

public class Student {
	 private String name;
	 private double[] score;
	 private double[] exam;

	 
	public Student(String name, double[] score, double[] exam) {
		this.name = name;
		this.score = score;
		this.exam = exam;
	}

	public String getName() {
		return name;
	}

	public double scoreAverage(double[] score){
		double scoreSum = 0;
		for (double d : score) {
			scoreSum += d;
		}
		double scoreAverage = scoreSum/5;
		return scoreAverage;
	}
	
	public double examAverage(double[] exam){
		double examSum = 0;
		for (double d : exam) {
			examSum += d;
		}
		double examAverage = examSum/2;
		return examAverage;
	}
}
