package examScore;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class HomeWork_Average_Exam {
	ArrayList<Student> students = new ArrayList<Student>();
	ArrayList<String> scoreAverage = new ArrayList<String>();
	ArrayList<String> examAverage = new ArrayList<String>();
	Student student;
	
	public HomeWork_Average_Exam() {
		String filename = "average.txt";
		String filename2 = "exam.txt";
		String name = "";
		double[] score = new double[5];
		double[] exam = new double[2];
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			
			FileReader fileReader2 = new FileReader(filename2);
			BufferedReader buffer2 = new BufferedReader(fileReader2);
			
			String line;
			String line2;
			
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				name = data[0].trim();
				score[0] = Double.parseDouble(data[1].trim());
				score[1] = Double.parseDouble(data[2].trim());
				score[2] = Double.parseDouble(data[3].trim());
				score[3] = Double.parseDouble(data[4].trim());
				score[4] = Double.parseDouble(data[5].trim());
				
				student = new Student(name, score, exam);
				scoreAverage.add(student.scoreAverage(score)+"");
			}
			
			for (line2 = buffer2.readLine(); line2 != null; line2 = buffer2.readLine()) {
				String[] data = line2.split(",");
				name = data[0].trim();
				exam[0] = Double.parseDouble(data[1].trim());
				exam[1] = Double.parseDouble(data[2].trim());
				
				student = new Student(name, score, exam);
				students.add(student);
				examAverage.add(student.examAverage(exam)+"");
			}	
			
			// write to file
			FileWriter fileWriter = new FileWriter("average.txt", true);
			PrintWriter out = new PrintWriter(new BufferedWriter(fileWriter));
			out.println(toStringExam());
			out.flush();
			
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}	
	}
	
	public String toString(){
		String str = "";
		for (int i = 0; i < students.size(); i++) {
			str += students.get(i).getName() + " " + scoreAverage.get(i) + "\n";
		}
		return str;
	}
	
	public String toStringExam(){
		String str = "";
		for (int i = 0; i < students.size(); i++) {
			str += students.get(i).getName() + " " + examAverage.get(i) + "\n";
		}
		return str;
	}
}
