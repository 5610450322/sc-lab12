package examScore;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class HomeWorkMain {

	public static void main(String[] args) {
		HomeWork_Average_Exam average_Exam = new HomeWork_Average_Exam();
		System.out.println("--------- Homework Scores ---------");
		System.out.println("Name Average");
		System.out.println("==== =======");
		System.out.println(average_Exam.toString());
		
		System.out.println("--------- Exam Scores ---------");
		System.out.println("Name Average");
		System.out.println("==== =======");
		System.out.println(average_Exam.toStringExam());
		
		
	}

}
