package homeworkScore;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class HomeWorkAverage {
	ArrayList<Student> students = new ArrayList<Student>();
	ArrayList<String> average = new ArrayList<String>();
	Student student;
	
	public HomeWorkAverage() {
		String filename = "average.txt";
		String name = "";
		double[] score = new double[5];
		try{
			FileReader fileReader = new FileReader(filename);
			BufferedReader buffer = new BufferedReader(fileReader);
			String line;
			for (line = buffer.readLine(); line != null; line = buffer.readLine()) {
				String[] data = line.split(",");
				name = data[0].trim();
				score[0] = Double.parseDouble(data[1].trim());
				score[1] = Double.parseDouble(data[2].trim());
				score[2] = Double.parseDouble(data[3].trim());
				score[3] = Double.parseDouble(data[4].trim());
				score[4] = Double.parseDouble(data[5].trim());
				
				student = new Student(name, score);
				students.add(student);
				average.add(student.scoreAverage(score)+"");
			}
			
		}
		catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}	 	
		catch (IOException e){
			System.err.println("Error reading from file");
		}	
	}
	
	public String toString(){
		String str = "";
		for (int i = 0; i < students.size(); i++) {
			str += students.get(i).getName() + " " + average.get(i) + "\n";
		}
		return str;
	}
}
