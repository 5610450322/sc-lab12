package homeworkScore;

public class Student {
	 private String name;
	 private double[] score;
	 
	public Student(String name, double[] score) {
		this.name = name;
		this.score = score;
	}

	public String getName() {
		return name;
	}

	public double scoreAverage(double[] score){
		double scoreSum = 0;
		for (double d : score) {
			scoreSum += d;
		}
		double scoreAverage = scoreSum/5;
		return scoreAverage;
	}
	
}
